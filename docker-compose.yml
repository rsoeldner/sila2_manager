version: "3.3"
services:

  proxy:
    image: traefik:v2.2
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    command:
      # Enable Docker in Traefik, so that it reads labels from Docker services
      - --providers.docker
      # Add a constraint to only use services with the label for this stack
      # from the env var TRAEFIK_TAG
      - --providers.docker.constraints=Label(`traefik.constraint-label-stack`, `${TRAEFIK_TAG?Variable not set}`)
      # Do not expose all Docker services, only the ones explicitly exposed
      - --providers.docker.exposedbydefault=false
      # Enable Docker Swarm mode
      - --providers.docker.swarmmode
      # Enable the access log, with HTTP requests
      - --accesslog
      # Enable the Traefik log, for configurations and errors
      - --log
      # Enable the Dashboard and API
      - --api
    deploy:
      placement:
        constraints:
          - node.role == manager
      labels:
        # Enable Traefik for this service, to make it available in the public network
        - traefik.enable=true
        # Use the traefik-public network (declared below)
        - traefik.docker.network=${TRAEFIK_PUBLIC_NETWORK?Variable not set}
        # Use the custom label "traefik.constraint-label=traefik-public"
        # This public Traefik will only use services with this label
        - traefik.constraint-label=${TRAEFIK_PUBLIC_TAG?Variable not set}
        # traefik-http set up only to use the middleware to redirect to https
        - traefik.http.middlewares.${STACK_NAME?Variable not set}-https-redirect.redirectscheme.scheme=https
        - traefik.http.middlewares.${STACK_NAME?Variable not set}-https-redirect.redirectscheme.permanent=true
        # Handle host with and without "www" to redirect to only one of them
        # Uses environment variable DOMAIN
        # To disable www redirection remove the Host() you want to discard, here and
        # below for HTTPS
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-http.rule=Host(`${DOMAIN?Variable not set}`) || Host(`www.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-http.entrypoints=http
        # traefik-https the actual router using HTTPS
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-https.rule=Host(`${DOMAIN?Variable not set}`) || Host(`www.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-https.entrypoints=https
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-https.tls=true
        # Use the "le" (Let's Encrypt) resolver created below
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-https.tls.certresolver=le
        # Define the port inside of the Docker service to use
        - traefik.http.services.${STACK_NAME?Variable not set}-proxy.loadbalancer.server.port=80
        # Handle domain with and without "www" to redirect to only one
        # To disable www redirection remove the next line
        - traefik.http.middlewares.${STACK_NAME?Variable not set}-www-redirect.redirectregex.regex=^https?://(www.)?(${DOMAIN?Variable not set})/(.*)
        # Redirect a domain with www to non-www
        # To disable it remove the next line
        - traefik.http.middlewares.${STACK_NAME?Variable not set}-www-redirect.redirectregex.replacement=https://${DOMAIN?Variable not set}/$${3}
        # Redirect a domain without www to www
        # To enable it remove the previous line and uncomment the next
        # - traefik.http.middlewares.${STACK_NAME}-www-redirect.redirectregex.replacement=https://www.${DOMAIN}/$${3}
        # Middleware to redirect www, to disable it remove the next line 
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-https.middlewares=${STACK_NAME?Variable not set}-www-redirect
        # Middleware to redirect www, and redirect HTTP to HTTPS
        # to disable www redirection remove the section: ${STACK_NAME?Variable not set}-www-redirect,
        - traefik.http.routers.${STACK_NAME?Variable not set}-proxy-http.middlewares=${STACK_NAME?Variable not set}-www-redirect,${STACK_NAME?Variable not set}-https-redirect

  db-backend-gateway:
    image: postgres:12
    volumes:
      - app-db-backend-gateway-data:/var/lib/postgresql/data/pgdata/backend-gateway/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/backend-gateway
      - POSTGRES_SERVER=${POSTGRES_SERVER_BACKEND_GATEWAY}
      - POSTGRES_DB=${POSTGRES_DB_BACKEND_GATEWAY}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-data == true

  db-workflow-designer-python:
    image: postgres:12
    volumes:
      - app-db-workflow-designer-python-data:/var/lib/postgresql/data/pgdata/workflow-designer-python/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/workflow-designer-python
      - POSTGRES_SERVER=${POSTGRES_SERVER_WORKFLOW_DESIGNER_PYTHON}
      - POSTGRES_DB=${POSTGRES_DB_WORKFLOW_DESIGNER_PYTHON}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-workflow-designer-python-data == true

  db-service-manager:
    image: postgres:12
    volumes:
      - app-db-service-manager-data:/var/lib/postgresql/data/pgdata/service-manager/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/service-manager
      - POSTGRES_SERVER=${POSTGRES_SERVER_SERVICE_MANAGER}
      - POSTGRES_DB=${POSTGRES_DB_SERVICE_MANAGER}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-data == true

  db-workflow-designer-node-red:
    image: postgres:12
    volumes:
      - app-db-workflow-designer-node-red-data:/var/lib/postgresql/data/pgdata/workflow-designer-node-red/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/workflow-designer-node-red
      - POSTGRES_SERVER=${POSTGRES_SERVER_WORKFLOW_DESIGNER_NODE_RED}
      - POSTGRES_DB=${POSTGRES_DB_WORKFLOW_DESIGNER_NODE_RED}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-workflow-designer-node-red-data == true

  db-workflow-scheduler:
    image: postgres:12
    volumes:
      - app-db-workflow-scheduler-data:/var/lib/postgresql/data/pgdata/workflow-scheduler/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/workflow-scheduler
      - POSTGRES_SERVER=${POSTGRES_SERVER_WORKFLOW_SCHEDULER}
      - POSTGRES_DB=${POSTGRES_DB_WORKFLOW_SCHEDULER}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-data == true

  db-data-acquisition:
    image: postgres:12
    volumes:
      - app-db-data-acquisition-data:/var/lib/postgresql/data/pgdata/data-acquisition/
    env_file:
      - .env
    environment:
      - PGDATA=/var/lib/postgresql/data/pgdata/data-acquisition
      - POSTGRES_SERVER=${POSTGRES_SERVER_DATA_ACQUISITION}
      - POSTGRES_DB=${POSTGRES_DB_DATA_ACQUISITION}
    deploy:
      placement:
        constraints:
          - node.labels.${STACK_NAME?Variable not set}.app-db-data == true

  pgadmin:
    image: dpage/pgadmin4
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    depends_on:
      - db-backend-gateway
      - db-service-manager
    env_file:
      - .env
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=${TRAEFIK_PUBLIC_NETWORK?Variable not set}
        - traefik.constraint-label=${TRAEFIK_PUBLIC_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-http.rule=Host(`pgadmin.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-http.entrypoints=http
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-http.middlewares=${STACK_NAME?Variable not set}-https-redirect
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-https.rule=Host(`pgadmin.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-https.entrypoints=https
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-https.tls=true
        - traefik.http.routers.${STACK_NAME?Variable not set}-pgadmin-https.tls.certresolver=le
        - traefik.http.services.${STACK_NAME?Variable not set}-pgadmin.loadbalancer.server.port=5050

  queue:
    image: rabbitmq:3
    # Using the below image instead is required to enable the "Broker" tab in the flower UI:
    # image: rabbitmq:3-management
    #
    # You also have to change the flower command
  
  flower:
    image: mher/flower:0.9.7
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    env_file:
      - .env
    command:
      - "--broker=amqp://guest@queue:5672//"
      # For the "Broker" tab to work in the flower UI, uncomment the following command argument,
      # and change the queue service's image as well
      # - "--broker_api=http://guest:guest@queue:15672/api//"
    deploy:
      labels:
        - traefik.enable=true
        - traefik.docker.network=${TRAEFIK_PUBLIC_NETWORK?Variable not set}
        - traefik.constraint-label=${TRAEFIK_PUBLIC_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-http.rule=Host(`flower.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-http.entrypoints=http
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-http.middlewares=${STACK_NAME?Variable not set}-https-redirect
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-https.rule=Host(`flower.${DOMAIN?Variable not set}`)
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-https.entrypoints=https
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-https.tls=true
        - traefik.http.routers.${STACK_NAME?Variable not set}-flower-https.tls.certresolver=le
        - traefik.http.services.${STACK_NAME?Variable not set}-flower.loadbalancer.server.port=5555


  backend-gateway:
    image: '${DOCKER_IMAGE_BACKEND_GATEWAY?Variable not set}:${TAG-latest}'
    networks:
        - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
        - default
    #ports:
     # - "${DOMAIN?Variable not set}:${BACKEND_GATEWAY_UVICORN_PORT?Variable not set}:${BACKEND_GATEWAY_UVICORN_PORT?Variable not set}"
    depends_on:
      - db-backend-gateway
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST}
      - HOST=0.0.0.0
      - PORT=${BACKEND_GATEWAY_UVICORN_PORT?Variable not set}
    build:
      context: ./backend-gateway
      dockerfile: backend-gateway.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=true
        - traefik.port=${BACKEND_GATEWAY_UVICORN_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-backend-gateway-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-backend-gateway.loadbalancer.server.port=${BACKEND_GATEWAY_UVICORN_PORT?Variable not set}

  celeryworker-backend-gateway:
    image: '${DOCKER_IMAGE_CELERYWORKER_BACKEND_GATEWAY?Variable not set}:${TAG-latest}'
    depends_on:
      - queue
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST?Variable not set}
    build:
      context: ./backend-gateway
      dockerfile: celeryworker.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}

  service-manager:
    image: '${DOCKER_IMAGE_SERVICE_MANAGER?Variable not set}:${TAG-latest}'
    ports:
      - "82:82"
      #- "${SERVICE_MANAGER_UVICORN_PORT?Variable not set}:${SERVICE_MANAGER_UVICORN_PORT?Variable not set}"
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    depends_on:
       - db-service-manager
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST}
      - HOST=0.0.0.0
      - PORT=${SERVICE_MANAGER_UVICORN_PORT?Variable not set}
      - BIND=0.0.0.0:${SERVICE_MANAGER_UVICORN_PORT?Variable not set}:80
    build:
      context: ./service-manager
      dockerfile: service-manager.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=false
        - traefik.port=${SERVICE_MANAGER_UVICORN_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-service-manager-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-service-manager.loadbalancer.server.port=${SERVICE_MANAGER_UVICORN_PORT?Variable not set}

  mdns_repeater:
    image: jdbeeler/mdns-repeater:latest
    # image: raetha/mdns-repeater
    network_mode: "host"
    privileged: true
    env_file:
      - ./service-manager/mdns-repeater.conf
    volumes:
      - //var/run/docker.sock:/var/run/docker.sock
    deploy:
      labels:
        - traefik.enable=false
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-service-manager-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-service-manager.loadbalancer.server.port=${SERVICE_MANAGER_UVICORN_PORT?Variable not set}

  celeryworker-service-manager:
    image: '${DOCKER_IMAGE_CELERYWORKER_SERVICE_MANAGER?Variable not set}:${TAG-latest}'
    depends_on:
      - db-service-manager
      - queue
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST?Variable not set}
    build:
      context: ./service-manager
      dockerfile: celeryworker.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}

  workflow-designer-python:
    image: '${DOCKER_IMAGE_WORKFLOW_DESIGNER_PYTHON?Variable not set}:${TAG-latest}'
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    ports:
      - "83:83"
      #  - "${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}:${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}"
    depends_on:
      - db-workflow-designer-python
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - HOST=0.0.0.0
      - PORT=${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}
      - BIND=0.0.0.0:${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}:80
    build:
      context: ./workflow-designer-python
      dockerfile: workflow-designer-python.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=false
        - traefik.port=${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-workflow-designer-python-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-workflow-designer-python.loadbalancer.server.port=${WORKFLOW_DESIGNER_PYTHON_UVICORN_PORT?Variable not set}

  celeryworker-workflow-designer-python:
    image: '${DOCKER_IMAGE_CELERYWORKER_WORKFLOW_DESIGNER_PYTHON?Variable not set}:${TAG-latest}'
    depends_on:
      - db-workflow-designer-python
      - queue
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      - DB_HOST=${POSTGRES_DB_WORKFLOW_DESIGNER_PYTHON?Variable not set}
      - DB_USER=${POSTGRES_USER?Variable not set}
      - DB_PASSWORD=${POSTGRES_PASSWORD?Variable not set}
      - DB_SERVER=${POSTGRES_SERVER_WORKFLOW_DESIGNER_PYTHON?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST?Variable not set}
    build:
      context: ./workflow-designer-python
      dockerfile: celeryworker.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}

  workflow-scheduler:
    image: '${DOCKER_IMAGE_WORKFLOW_SCHEDULER}:${TAG-latest}'
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    ports:
      - "84:84"
      #- "${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}:${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}"
    depends_on:
      - db-workflow-scheduler
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST}
      - HOST=0.0.0.0
      - PORT=${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}
      - BIND=0.0.0.0:${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}:80
    build:
      context: ./workflow-scheduler
      dockerfile: workflow-scheduler.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=false
        - traefik.port=${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-workflow-scheduler-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-workflow-scheduler.loadbalancer.server.port=${WORKFLOW_SCHEDULER_UVICORN_PORT?Variable not set}

  workflow-executor-node-red:
    image: '${DOCKER_IMAGE_WORKFLOW_EXECUTOR_NODE_RED?Variable not set}:${TAG-latest}'
    depends_on:
      - db-workflow-designer-node-red
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
    ports:
      - "2880:1880"
    build:
      context: ./workflow-executor-node-red
      dockerfile: workflow-executor-node-red.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=true
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}

  celeryworker-workflow-scheduler:
    image: '${DOCKER_IMAGE_CELERYWORKER_WORKFLOW_SCHEDULER?Variable not set}:${TAG-latest}'
    depends_on:
      - db-workflow-scheduler
      - queue
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST?Variable not set}
    build:
      context: ./workflow-scheduler
      dockerfile: celeryworker.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}

  data-acquisition:
    image: '${DOCKER_IMAGE_DATA_ACQUISITION?Variable not set}:${TAG-latest}'
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    ports:
      - "${DATA_ACQUISITION_UVICORN_PORT?Variable not set}:${DATA_ACQUISITION_UVICORN_PORT?Variable not set}"
    depends_on:
      - db-data-acquisition
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST}
      - HOST=0.0.0.0
      - PORT=${DATA_ACQUISITION_UVICORN_PORT?Variable not set}
      - BIND=0.0.0.0:${DATA_ACQUISITION_UVICORN_PORT?Variable not set}:80
    build:
      context: ./data-acquisition
      dockerfile: data-acquisition.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=false
        - traefik.port=${DATA_ACQUISITION_UVICORN_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-data-acquisition-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-data-acquisition.loadbalancer.server.port=${DATA_ACQUISITION_UVICORN_PORT?Variable not set}

  celeryworker-data-acquisition:
    image: '${DOCKER_IMAGE_CELERYWORKER_DATA_ACQUISITION?Variable not set}:${TAG-latest}'
    depends_on:
      - db-data-acquisition
      - queue
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      # Allow explicit env var override for tests
      - SMTP_HOST=${SMTP_HOST?Variable not set}
    build:
      context: ./data-acquisition
      dockerfile: celeryworker.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}

  frontend:
    image: '${DOCKER_IMAGE_FRONTEND?Variable not set}:${TAG-latest}'
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    build:
      context: ./frontend
      dockerfile: Dockerfile
      #args:
      #FRONTEND_ENV: ${FRONTEND_ENV-production}
    volumes:
      - ./frontend:/app/
      - /app/node_modules
    ports:
     - "4200:4200"
    deploy:
      labels:
        - traefik.enable=true
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-frontend-http.rule=PathPrefix(`/`)
        - traefik.http.services.${STACK_NAME?Variable not set}-frontend.loadbalancer.server.port=80

  workflow-designer-node-red:
    image: '${DOCKER_IMAGE_WORKFLOW_DESIGNER_NODE_RED?Variable not set}:${TAG-latest}'
    networks:
      - ${TRAEFIK_PUBLIC_NETWORK?Variable not set}
      - default
    ports:
      - "1880:1880"
      - "${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}:${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}"
    depends_on:
      - db-workflow-designer-node-red
    env_file:
      - .env
    environment:
      - SERVER_NAME=${DOMAIN?Variable not set}
      - SERVER_HOST=https://${DOMAIN?Variable not set}
      - DB_HOST=${POSTGRES_DB_WORKFLOW_DESIGNER_NODE_RED?Variable not set}
      - DB_USER=${POSTGRES_USER?Variable not set}
      - DB_PASSWORD=${POSTGRES_PASSWORD?Variable not set}
      - DB_SERVER=${POSTGRES_SERVER_WORKFLOW_DESIGNER_NODE_RED?Variable not set}
      # Allow explicit env var override for tests
      - PORT=${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}
      - BIND=${DOMAIN?Variable not set}:${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}
    build:
      context: ./workflow-designer-node-red
      dockerfile: workflow-designer-node-red.dockerfile
      args:
        INSTALL_DEV: ${INSTALL_DEV-false}
    deploy:
      labels:
        - traefik.enable=false
        - traefik.port=${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}
        - traefik.constraint-label-stack=${TRAEFIK_TAG?Variable not set}
        - traefik.http.routers.${STACK_NAME?Variable not set}-workflow-designer-node-red-http.rule=PathPrefix(`/api`) || PathPrefix(`/docs`) || PathPrefix(`/redoc`)
        - traefik.http.services.${STACK_NAME?Variable not set}-workflow-designer-node-red.loadbalancer.server.port=1880
        - traefik.http.services.${STACK_NAME?Variable not set}-workflow-designer-node-red.loadbalancer.server.port=${WORKFLOW_DESIGNER_NODE_RED_PORT?Variable not set}

volumes:
  app-db-backend-gateway-data:
  app-db-service-manager-data:
  app-db-workflow-designer-node-red-data:
  app-db-workflow-designer-python-data:
  app-db-workflow-scheduler-data:
  app-db-data-acquisition-data:

networks:
  traefik-public:
    # Allow setting it to false for testing
    external: ${TRAEFIK_PUBLIC_NETWORK_IS_EXTERNAL-true}
