"""initial commit

Revision ID: 9dc6cc2eed27
Revises: 
Create Date: 2022-02-04 12:47:42.403471

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9dc6cc2eed27'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('full_name', sa.String(), nullable=True),
    sa.Column('email', sa.String(), nullable=False),
    sa.Column('hashed_password', sa.String(), nullable=False),
    sa.Column('is_active', sa.Boolean(), nullable=True),
    sa.Column('is_superuser', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)
    op.create_index(op.f('ix_user_full_name'), 'user', ['full_name'], unique=False)
    op.create_index(op.f('ix_user_id'), 'user', ['id'], unique=False)
    op.create_table('workflow',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('title', sa.String(), nullable=True),
    sa.Column('workflow_type', sa.String(), nullable=True),
    sa.Column('file_name', sa.String(), nullable=True),
    sa.Column('data', sa.Text(), nullable=True),
    sa.Column('description', sa.String(), nullable=True),
    sa.Column('owner', sa.String(), nullable=True),
    sa.Column('owner_id', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_workflow_description'), 'workflow', ['description'], unique=False)
    op.create_index(op.f('ix_workflow_file_name'), 'workflow', ['file_name'], unique=False)
    op.create_index(op.f('ix_workflow_id'), 'workflow', ['id'], unique=False)
    op.create_index(op.f('ix_workflow_owner'), 'workflow', ['owner'], unique=False)
    op.create_index(op.f('ix_workflow_owner_id'), 'workflow', ['owner_id'], unique=False)
    op.create_index(op.f('ix_workflow_title'), 'workflow', ['title'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_workflow_title'), table_name='workflow')
    op.drop_index(op.f('ix_workflow_owner_id'), table_name='workflow')
    op.drop_index(op.f('ix_workflow_owner'), table_name='workflow')
    op.drop_index(op.f('ix_workflow_id'), table_name='workflow')
    op.drop_index(op.f('ix_workflow_file_name'), table_name='workflow')
    op.drop_index(op.f('ix_workflow_description'), table_name='workflow')
    op.drop_table('workflow')
    op.drop_index(op.f('ix_user_id'), table_name='user')
    op.drop_index(op.f('ix_user_full_name'), table_name='user')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    # ### end Alembic commands ###
